class ApplicationMailer < ActionMailer::Base
  default from: ENV["smtp_gmail_user_name"]
  layout 'mailer'
  


end
