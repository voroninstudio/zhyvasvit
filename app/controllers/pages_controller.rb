class PagesController < ApplicationController
  skip_before_action :verify_authenticity_token
  def index
    @main_page = MainPage.first_or_initialize
    @articles = Article.order_by_date_desc
    set_page_metadata(:home)
    set_local_business_microdata
  end

  def about_school
    @about_school = AboutSchool.first_or_initialize
    set_page_metadata(:about_school)
  end

  def program
    @program = Program.first_or_initialize
    @articles = Article.order_by_date_desc
    set_page_metadata(:program)
  end

  def methodology
    @methodology = Methodology.first_or_initialize
    set_page_metadata(:methodology)
  end

 def blog
    @blog = Blog.first_or_initialize
    @articles = Article.order_by_date_desc
    @subscriber = Subscriber.new
    set_page_metadata(:articles)
 end

 def contacts
    @articles = Article.order_by_date_desc
    set_page_metadata(:contacts)
 end

 def dietology
  @dietology = Dietology.first_or_initialize
  @articles = Article.order_by_date_desc
  set_page_metadata(:dietology)
 end

 def kinezoterapy
  @kinezoterapy = Kinezoterapy.first_or_initialize
  @articles = Article.order_by_date_desc
  set_page_metadata(:kinesiotherapy)
 end

 def manual
   @manual = Manual.first_or_initialize
   @articles = Article.order_by_date_desc
   set_page_metadata(:manual_therapy)
 end

 def kosmetology
   @kosmetology = Kosmetology.first_or_initialize
   @articles = Article.order_by_date_desc
   set_page_metadata(:cosmetology)
 end

 def psychoterapy
    @psychoterapy = Psychoterapy.first_or_initialize
    @articles = Article.order_by_date_desc
    set_page_metadata(:psychotherapy)
 end

 def article
    @article = Article.where(url_fragment: params[:id]).first
    return render_not_found unless @article

    @articles = @article.next(Article.order_by_date_desc, count: 3, except_self: true)
    set_page_metadata(@article)
    set_article_microdata
 end

 def klimatoterapy
   @klimatoterapy = Klimatoterapy.first_or_initialize
   @articles = Article.order_by_date_desc
   set_page_metadata(:climatotherapy)
 end

  def consultation

    @consultation = {
      klients_name: params[:klients_name],
      phone_number: params[:phone_number],
      mail: params[:mail],

      select: params[:select]
    }

      redirect_to root_path

    UserMailer.consultation_email(params).deliver_now

    render json: {}
  end


end