class SubscribersController < ApplicationController
  def create
    @subscriber = Subscriber.new(subscriber_params)
    @subscriber.save
    redirect_to root_path
  end


 #  def send_email(email,article)    
	#   @article = article
	#   mail(to: Subscriber.pluck(:email) , subject: @article.title)
	#   UserMailer.send_email(@subscriber,@articles).deliver
	# end

  private

  def subscriber_params
    params.require(:subscriber).permit( :email)
  end
end