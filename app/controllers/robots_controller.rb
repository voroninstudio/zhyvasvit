class RobotsController < Cms::RobotsController
  def robots_txt
    #@lines = lines

    configure_robots_txt do
      if robots_txt_production?
        user_agent '*' do
          if disallow_locales?
            disallowed_locales.each do |locale|
              disallow "/#{locale}"
            end
          else
            disallow ''
            #disallow '/admin'
            #disallow '/users'
          end
          sitemap
          host
        end
      else
        user_agent '*' do
          disallow '/'
          host
        end
      end
    end

    render inline: render_robots_txt_to_string
  end
end