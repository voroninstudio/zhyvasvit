class Api::ArticlesController < ApplicationController
	respond_to :json

	def index
    @articles = {
    	article: all_articles(Article.order_by_date_desc)
    }
    respond_with @articles

  end

  def show
   @article = Article.find(params[:id])
   # render json: @article
	end

  private

	def all_articles (articles)
		articles.map{ |article|
			{
				image: article.image,
				title: article.title,
				short_description: article.short_description,
				describe: article.describe,
				date: article.date,
				category: article.category,
				video: article.video
			}
		}
	end

end