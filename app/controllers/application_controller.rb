class ApplicationController < ActionController::Base

  include ActionView::Helpers::OutputSafetyHelper
  include Cms::Helpers::ImageHelper
  include ActionView::Helpers::AssetUrlHelper
  include ActionView::Helpers::TagHelper
  include ActionView::Helpers::UrlHelper
  include Cms::Helpers::UrlHelper
  include Cms::Helpers::PagesHelper
  include Cms::Helpers::MetaDataHelper
  include Cms::Helpers::NavigationHelper
  include Cms::Helpers::ActionView::UrlHelper
  include Cms::Helpers::Breadcrumbs
  include ActionControllerExtensions::InstanceMethods
  include ApplicationHelper
  include Cms::Helpers::AnotherFormsHelper
  include Cms::Helpers::TagsHelper
  include Cms::Helpers::AssetHelper
  reload_rails_admin_config
  initialize_locale_links

  before_action :set_default_og_image

  def root_without_locale
    redirect_to root_path(locale: I18n.locale)
  end

  def render_not_found
    render template: "errors/not_found.html.slim", status: 404, layout: "application"
  end

  protected

  def set_default_og_image
    @default_og_image = absolute_url(asset_path("photo/New_logo.png"))
  end

  def set_microdata(key, data)
    @micro_data ||= {}
    @micro_data[key] = data
  end

  def set_local_business_microdata
    host = absolute_url("/")
    logo_url = absolute_url(asset_path("photo/New_logo.png"))

    data = {
      "@context": "https://schema.org",
      "@type": "HealthAndBeautyBusiness",
      "name": "Живасвіт",
      "image": logo_url,
      "@id": host,
      "url": host,
      "telephone": '+38 (096) 820 90 59',
      "geo": {
        "@type": "GeoCoordinates",
        "latitude": 49.831887,
        "longitude": 24.047567
      },
      "address": {
        "@type": "PostalAddress",
        "streetAddress": "вул. Мечнікова 20",
        "addressLocality": "Львів",
        "postalCode": "79017",
        "addressCountry": "UA"
      }
    }

    set_microdata(:local_business, data)
  end

  def set_article_microdata
    return unless @page_instance
    logo_url = absolute_url(asset_path("photo/New_logo.png"))

    data = {
      "@context": "https://schema.org",
      "@type": "Article",
      "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": absolute_url(@page_instance.url)
      },
      "headline": head_title,
      "description": meta_description,
      "image": og_image,
      "author": {
        "@type": "Person",
        "name": "Ігор"
      },
      "publisher": {
        "@type": "Organization",
        "name": "Ігор",
        "logo": {
          "@type": "ImageObject",
          "url": logo_url
        }
      },
      "datePublished": @page_instance.date.strftime('%Y-%m-%d'),
      "dateModified": @page_instance.updated_at.strftime("%Y-%m-%dT%H:%M:%S%z")
    }

    set_microdata(:article, data)
  end
end
