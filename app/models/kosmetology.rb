class Kosmetology < ActiveRecord::Base
	attr_accessible *attribute_names

	mount_uploader :main_image, ImageUploader
	mount_uploader :fiziology_image, ImageUploader
	mount_uploader :rekonstruktive_image, ImageUploader
	mount_uploader :third_image, ImageUploader
end