class Pages::AboutSchool < Cms::Page
  include Cms::LocalizedRoutes::UrlHelper::ActiveRecordExtension

  def url(locale = I18n.locale)
    url_helpers.about_school_path
  end

  def og_image_url
    AboutSchool.first_or_initialize.main_image.url.presence
  end
end