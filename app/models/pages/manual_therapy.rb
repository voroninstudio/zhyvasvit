class Pages::ManualTherapy < Cms::Page
  include Cms::LocalizedRoutes::UrlHelper::ActiveRecordExtension

  def url(locale = I18n.locale)
    url_helpers.manual_path
  end

  def og_image_url
    Manual.first_or_initialize.main_image.url.presence
  end
end