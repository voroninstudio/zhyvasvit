class Pages::Cosmetology < Cms::Page
  include Cms::LocalizedRoutes::UrlHelper::ActiveRecordExtension

  def url(locale = I18n.locale)
    url_helpers.kosmetology_path
  end

  def og_image_url
    Kosmetology.first_or_initialize.main_image.url.presence
  end
end