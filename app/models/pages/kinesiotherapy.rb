class Pages::Kinesiotherapy < Cms::Page
  include Cms::LocalizedRoutes::UrlHelper::ActiveRecordExtension

  def url(locale = I18n.locale)
    url_helpers.kinezoterapy_path
  end

  def og_image_url
    Kinezoterapy.first_or_initialize.main_image.url.presence
  end
end