class Pages::Articles < Cms::Page
  include Cms::LocalizedRoutes::UrlHelper::ActiveRecordExtension

  def url(locale = I18n.locale)
    url_helpers.blog_path
  end

  def og_image_url
    Blog.first_or_initialize.main_image.url.presence
  end
end