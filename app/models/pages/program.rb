class Pages::Program < Cms::Page
  include Cms::LocalizedRoutes::UrlHelper::ActiveRecordExtension

  def url(locale = I18n.locale)
    url_helpers.program_path
  end

  def og_image_url
    ::Program.first_or_initialize.main_image.url.presence
  end
end