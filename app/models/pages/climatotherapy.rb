class Pages::Climatotherapy < Cms::Page
  include Cms::LocalizedRoutes::UrlHelper::ActiveRecordExtension

  def url(locale = I18n.locale)
    url_helpers.klimatoterapy_path
  end

  def og_image_url
    Klimatoterapy.first_or_initialize.main_image.url.presence
  end
end