class Pages::Contacts < Cms::Page
  include Cms::LocalizedRoutes::UrlHelper::ActiveRecordExtension

  def url(locale = I18n.locale)
    url_helpers.contacts_path
  end
end