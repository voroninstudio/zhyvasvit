class Program < ActiveRecord::Base
	attr_accessible *attribute_names

	mount_uploader :main_image, ImageUploader
	mount_uploader :first_image, ImageUploader
	mount_uploader :a_p_image, ImageUploader
	mount_uploader :stage_1_image, ImageUploader
	mount_uploader :stage_2_image, ImageUploader
	mount_uploader :stage_3_image, ImageUploader
	mount_uploader :stage_4_image, ImageUploader
end

