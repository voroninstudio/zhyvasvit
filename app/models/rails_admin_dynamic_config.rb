module RailsAdminDynamicConfig
  class << self
    def configure_rails_admin(initial = true)
      RailsAdmin.config do |config|

        ### Popular gems integration

        ## == Devise ==
        config.authenticate_with do
          warden.authenticate! scope: :user
        end
        config.current_user_method(&:current_user)

        ## == Cancan ==
        #config.authorize_with :cancan

        ## == Pundit ==
        # config.authorize_with :pundit

        ## == PaperTrail ==
        # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

        ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration



        if initial
          config.actions do
            dashboard                     # mandatory
            index                         # mandatory
            new do
              #except [CallRequest, ConsultationRequest, MeterRequest, ContactsRequest, PartnershipRequest]
            end
            export
            bulk_delete
            show
            edit
            delete
            show_in_app
            props do
              only []
            end
            #edit_model
            nestable do
              only [Cms::Page]
            end

            ## With an audit adapter, you can add:
            # history_index
            # history_show
          end


          config.parent_controller = "ApplicationController"
        end

        config.navigation_labels do
          [:home, :blog, :methodology, :about_school, :program, :pages, :users, :settings]
        end

        # config.navigation_static_links = {
        #     mailchimp: "/admin/mailchimp",
        #     locales: "/file_editor/locales",
        #     site_data: "/file_editor/site_data.yml"
        # }


        config.include_models Cms::SitemapElement, Cms::MetaTags
        config.include_models Cms::Page
        config.model Cms::Page do
          navigation_label_key(:pages, 1)
          nestable_list({position_field: :sorting_position, scope: :order_by_sorting_position})
          object_label_method do
            :custom_name
            #{
            #k = @bindings[:object].type.underscore.split("/").last
            #I18n.t("activerecord.models.pages.#{k}", raise: true) rescue k.humanize
            #}
          end
          list do
            sort_by do
              "sorting_position"
            end

            field :name do
              def value
                k = @bindings[:object].type.underscore.split("/").last
                I18n.t("activerecord.models.pages.#{k}", raise: true) rescue k.humanize
              end
            end

            # field :h1_text do
            #   def value
            #     @bindings[:object].h1_text
            #   end
            # end
          end

          edit do
            field :name do
              read_only true
              def value
                k = @bindings[:object].type.underscore.split("/").last
                I18n.t("activerecord.models.pages.#{k}", raise: true) rescue k.humanize
              end
            end

            #field :translations, :globalize_tabs

            field :seo_tags

          end

        end

        config.model_translation Cms::Page do
          field :locale, :hidden
          field :h1_text
        end



        config.model Cms::MetaTags do
          visible false
          field :translations, :globalize_tabs
        end

        config.model_translation Cms::MetaTags do
          field :locale, :hidden
          field :title
          field :keywords
          field :description
        end


        config.model Cms::SitemapElement do
          visible false

          field :display_on_sitemap
          field :changefreq
          field :priority
        end

        # config.include_models Attachable::Asset
        #
        #
        # config.model Attachable::Asset do
        #   navigation_label_key(:assets, 1)
        #   field :data
        #   #watermark_position_field(:data)
        #   field :sorting_position
        #   field :translations, :globalize_tabs
        # end
        #
        # config.model_translation Attachable::Asset do
        #   field :locale, :hidden
        #   field :data_alt
        # end



        config.include_models User
        config.model User do
          navigation_label_key(:users, 1)
          field :email
          field :password
          field :password_confirmation
        end

        # config.include_models Cms::Tag, Cms::Tagging
        #
        # config.model Cms::Tag do
        #   navigation_label_key(:tags, 1)
        #
        #   field :translations, :globalize_tabs
        #
        #   field :videos
        # end
        #
        #
        # config.model_translation Cms::Tag do
        #   field :locale, :hidden
        #   field :name
        #   field :url_fragment do
        #     help do
        #       I18n.t("admin.help.#{name}")
        #     end
        #   end
        # end
        #
        #
        # config.model Cms::Tagging do
        #   visible false
        # end

        # ===================================================
        # Requests
        # ===================================================
        config.configure_forms(:all)

        # ===================================================
        # Application specific models
        # ===================================================

        # Include models

        config.include_models Blog,Article,Program,AboutSchool,MainPage
        config.include_models Methodology, Kosmetology, Klimatoterapy, Manual, Psychoterapy, Kinezoterapy, Dietology

        config.model Methodology do
          navigation_label_key(:methodology)
          label "Методологія"
          include_fields :main_image, :main_describe, :main_title, :first_title,
                         :first_description, :second_list_description, :task_title,
                         :task_description, :first_image, :second_title,
                         :dieto_title, :dieto_description, :kinezo_title, :kinezo_description, :manual_title,
                         :manual_description, :psyho_title, :psyho_description, :kosmetology_title, :kosmetology_description,
                         :klimatology_title, :klimatology_description
        end

        config.model Kinezoterapy do
          navigation_label_key(:methodology)
          label "Кінезотерапія"
          include_fields :main_image, :main_describe, :main_title, :first_title,
                         :first_description, :title_technicks, :first_technick_title,
                         :first_technick_describe, :first_list_title, :first_line_first,
                         :first_line_second, :first_line_third, :first_describe_end,
                         :first_image, :second_image, :second_title, :second_description,
                         :third_image, :third_title, :third_description
        end

        config.model Klimatoterapy do
          navigation_label_key(:methodology)
          label "Кліматотерапія"
          include_fields :main_image, :main_describe, :main_title, :first_title,
                         :first_description, :second_description, :third_description, :fourth_description
        end

        config.model Psychoterapy do
          navigation_label_key(:methodology)
          label "Психотерапія"
          include_fields :main_image, :main_describe, :main_title, :first_title,
                         :first_description, :list_title, :first_list_description,
                         :second_list_description, :third_list_description, :fourth_list_description
        end

        config.model Dietology do
          navigation_label_key(:methodology)
          label "Дієтотерапія"
          include_fields :main_image, :main_describe, :main_title, :first_title,
                         :main_title, :first_title, :first_description,
                         :second_description, :third_description, :first_title_list, :first_line_description_first,
                         :second_line_description_first, :third_line_description_first, :fourth_line_description_first,
                         :fifth_line_description_first, :six_line_description_first, :second_title_list, :first_line_description_second,
                         :second_line_description_second, :third_line_description_second, :fourth_line_description_second, :bottom_title
        end

        config.model Manual do
          navigation_label_key(:methodology)
          label "Мануальна терапія"
          include_fields :main_image, :main_describe, :main_title, :first_title,
                         :first_description, :title_technicks, :first_title_technicks,
                         :first_describe_technicks, :french_technick_title, :french_technick_first_line,
                         :french_technick_second_line, :french_technick_third_line, :first_image,
                         :second_image, :second_title, :second_description, :third_image,
                         :third_title, :third_description
        end

        config.model Kosmetology do
          navigation_label_key(:methodology)
          label "Косметологія"
          include_fields :main_image, :main_describe, :main_title, :first_title,
                         :first_description, :second_description, :third_description, :first_number,
                         :fiziology_title, :fiziology_describe, :innovation_title, :innovation_first_describe,
                         :innovation_second_describe, :fiziology_image, :rekonstruktive_image, :second_number,
                         :rekonstruktive_title, :rekonstruktive_describe, :botolino_title, :botolino_describe,
                         :gioluronov_acid_title, :gioluronov_acid_describe, :third_number,
                         :third_title, :third_describe, :third_image
        end

        config.model MainPage do
          navigation_label_key(:home)
          label "Головна"
          include_fields :main_title, :main_title_list, :first_line_list, :second_line_list,
                         :third_line_list, :first_image, :second_image, :why_we_sick_title, :why_we_sick_describe, :new_way_title,
                         :new_way_describe, :about_author_title, :authors_name, :authors_describe,
                         :video_title, :video, :ways_title, :ways_describe, :info_title, :info_describe,
                         :info_image, :practise_image, :practise_title, :practise_describe,
                         :help_title, :help_desciribe, :help_image, :feeling_title, :headach_title, :insomina_title,
                         :irritable_title, :pain_title, :pressure_title, :fatigue_title, :memory_title,
                         :depresion_title, :weight_title, :chronic_title, :chronic_describe, :how_to_work_title,
                         :konsultation_title, :konsultation_describe, :detoks_title, :detoks_describe, :koretion_title,
                         :koretion_describe, :restore_title, :restore_describe, :helping_title, :helping_describe,
                         :konsultation_title, :konsultation_describe, :feedback_title
        end

        config.model AboutSchool do
          navigation_label_key(:about_school)
          label "Про школу"
          include_fields :main_image, :main_describe, :main_title, :first_title,
                         :first_description, :first_image, :second_title, :second_description,
                         :third_title, :third_description, :fourth_title, :fourth_description,
                         :title_principes, :new_way_description, :base_title, :first_line_principes, :second_line_principes,
                         :third_line_principes, :fourth_line_principes, :fifth_line_principes, :second_image,
                         :title_school, :information, :information_title, :information_description,
                         :practice, :practice_title, :practice_description, :vacation, :vacation_title,
                         :vacation_description, :shypailo_name, :shypailo_describe, :shypailo_first_title,
                         :shypailo_first_describe, :shypailo_image, :shypailo_way, :help_title, :help_describe,
                         :step_one, :step_one_title, :step_one_describe, :step_two, :step_two_title,
                         :step_two_describe, :main_methodology, :dietoterapy_title, :dietoterapy_describe,
                         :kinezoterapy_title, :kinezoterapy_describe, :manual_title, :manual_describe, :psyhoterapy_title,
                         :psyhoterapy_describe
        end

        config.model Program do
          navigation_label_key(:program)
          label "Програма"
          include_fields :main_image, :main_describe, :main_title, :first_title,
                         :first_image, :first_title_description, :first_description, :second_title,
                         :second_description, :first_title_list, :first_line_list, :second_line_list,
                         :third_line_list, :fourth_line_list, :fifth_line_list,
                         :six_line_list, :seventh_line_list, :eight_line_list, :nine_line_list,
                         :ten_line_list, :eleven_line_list, :twelve_line_list, :third_title_right,
                         :button_describe, :about_program_title, :a_p_description, :a_p_first_title, :a_p_first_descrpiton,
                         :a_p_second_title, :a_p_second_descrpiton, :a_p_third_title, :a_p_third_descrpiton,
                         :a_p_image, :a_p_fourth_title, :step_one, :step_one_title, :step_one_describe,
                         :step_two, :step_two_title, :step_two_describe, :stages_title, :stages_describe, :stage_1,
                         :stage_1_title, :stage_1_first_line, :stage_1_second_line, :stage_1_third_line,:stage_1_image,
                         :stage_2, :stage_2_title, :stage_2_title, :stage_2_describe,:stage_2_image, :stage_3,
                         :stage_3_title, :stage_3_first_line, :stage_3_second_line, :stage_3_third_line,
                         :stage_3_describe,:stage_3_image, :stage_4, :stage_4_title, :stage_4_describe,:stage_4_image
        end

        config.model Blog do
          navigation_label_key(:blog)
          label "Блог"
          include_fields :main_image, :main_describe, :main_title,
                         :first_title, :first_describe, :first_category,
                         :second_category, :third_category, :fourth_category
        end

        config.model Article do
          navigation_label_key(:blog)
          label "Стаття"
          configure :describe, :ck_editor
          include_fields :image, :title, :url_fragment, :short_description, :describe,
                         :date, :category, :video, :seo_tags
        end



      end
    end
  end
end