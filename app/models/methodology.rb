class Methodology < ActiveRecord::Base
	attr_accessible *attribute_names

	mount_uploader :main_image, ImageUploader
	mount_uploader :first_image, ImageUploader
end