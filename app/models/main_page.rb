class MainPage < ActiveRecord::Base
	attr_accessible *attribute_names

	mount_uploader :first_image, ImageUploader
	mount_uploader :second_image, ImageUploader
	mount_uploader :info_image, ImageUploader
	mount_uploader :practise_image, ImageUploader
	mount_uploader :help_image, ImageUploader

end
