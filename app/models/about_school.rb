class AboutSchool < ActiveRecord::Base
	attr_accessible *attribute_names

	mount_uploader :main_image, ImageUploader
	mount_uploader :first_image, ImageUploader
	mount_uploader :second_image, ImageUploader
	mount_uploader :shypailo_image, ImageUploader
end