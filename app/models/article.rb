class Article < ActiveRecord::Base
  attr_accessible *attribute_names

  scope :order_by_date_desc, -> { order(date: :desc) }

	after_create :send_email_to_subscribers
	mount_uploader :image, ImageUploader

  has_seo_tags
  has_sitemap_record

  has_cache do
    pages :articles
    pages Article.all
  end

  before_save :initialize_url_fragment, :initialize_date

  def url(locale = I18n.locale)
    url_helpers.article_path(id: url_fragment)
  end

  def name
    title || ''
  end

  def formatted_date
    return nil if date.blank?

    date.strftime('%d.%m.%Y')
  end

  def initialize_date
    self.date = self.updated_at if date.blank?
  end

  def og_image_url
    image.url.presence
  end

	private
	  def send_email_to_subscribers
	  	puts "send_email_to_subscribers"
		  Subscriber.all.each do |subscriber|
			  UserMailer.send_email(subscriber.email,self).deliver_now
	    end
	  end
end

# dates by id:
# [[3, ""], [4, "19.04.2019"], [6, "09.08.2019"], [7, "09.08.2019"], [8, "27.08.2019"]]