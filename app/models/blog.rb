class Blog < ActiveRecord::Base
	attr_accessible *attribute_names

	mount_uploader :main_image, ImageUploader
end