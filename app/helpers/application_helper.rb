module ApplicationHelper
  def primary_phone
    '096 820 90 59'
  end

  def primary_phone_url
    'tel:+380968209059'
  end

  def secondary_phone
    '097 464 33 99'
  end

  def secondary_phone_url
    'tel:+380974643399'
  end

  def primary_email
    'zhyvasvit@gmail.com'
  end

  def primary_email_url
    "mailto:#{primary_email}"
  end

  def skype
    'zhyvasvit'
  end

  def skype_url
    "skype:#{skype}?call"
  end

  def google_map_iframe_url
    "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2573.5625372992413!2d24.04629006720437!3d49.83188677939485!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473add44c10b47ff%3A0xa617e9eacbf43af7!2sMechnykova+St%2C+20%2C+L&#39;viv%2C+L&#39;vivs&#39;ka+oblast%2C+79000!5e0!3m2!1sen!2sua!4v1557837911887!5m2!1sen!2sua".html_safe
  end

  def facebook_url
    'https://www.facebook.com/zhyvasvit/'
  end

  def instagram_url
    'https://www.instagram.com/zhyvasvit/'
  end
end
