$document = $(document)

is_touch_screen = 'ontouchstart' in document.documentElement;
width = window.innerWidth;


///// відкриття-закриття хедера і блокування скролінгу
$('#toggle').click(function() {
   $(this).toggleClass('active');
   $('#overlay').css("display", "block");
   $('body').css("overflow", "hidden");
  });

$('#header_top_right').click(function() {
   $(this).toggleClass('active');
   $('#overlay').css("display", "none");
   $('body').css("overflow", "auto")
 });

 ////анімація на головній сторінці 5-ти кіл різнокольорових
$(document).ready(function() {
	var windowHeight = $(window).height();

	$(document).on('scroll', function() {
		$('.single-chart').each(function() {
			var self = $(this),
			height = self.offset().top + self.height();
			if ($(document).scrollTop() + windowHeight >= height) {
				$('.circle').css("animation", "progress 2s ease-out forwards");

			}
		});
	});
});

//////  хедер відкритий- стрілочка методик
$('#arrow').click(function(){
	$(this).toggleClass('active');
  $('#dropdown_content').css("display", "block");
  $('#arrow').css("display", "none");
  $('#arrow_1').css("display", "block");
});
$('#arrow_1').click(function(){
	$(this).toggleClass('active');
  $('#dropdown_content').css("display", "none");
  $('#arrow').css("display", "block");
  $('#arrow_1').css("display", "none");
});

$('.rew').hover(function(){
  $('#dropdown_content').css("display", "block");
  $('#arrow').css("display", "none");
  $('#arrow_1').css("display", "block");
},function(){
  $('#dropdown_content').css("display", "none");
  $('#arrow').css("display", "block");
  $('#arrow_1').css("display", "none");
})



//////hover на методики в хедері
$('#dropdown_content').hover(function(){
  $('#dropdown_content').css("display", "block");
},function(){
  $('#dropdown_content').css("display", "none");
})




////// анімації СВГ довкола фотографій
$(".main" ).scroll(function() {
var theta = $(".main" ).scrollTop() / 1800 % Math.PI;
$('.top_orange_circle').css({ transform: 'rotate(' + theta + 'rad)' });
});

$(".main" ).scroll(function() {
var theta = $(".main" ).scrollTop() / 1800 % Math.PI;
$('.practice_circle').css({ transform: 'rotate(' + theta + 'rad)' });
});

$(".main" ).scroll(function() {
var theta = $(".main" ).scrollTop() /2400 % Math.PI;
// $('.green_circl').css({ transform: 'rotate(' + theta + 'rad)' });
});







//////ховається-вспливає верхнє меню з "консультацією" менюшкою
var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("header_content").style.top = "0";
    document.getElementById("qwert").style.backgroundColor = "white";
  } else {
    document.getElementById("header_content").style.top = "-150px";
  }
  prevScrollpos = currentScrollPos;
}

///// about-school - відкриття/закриття фотографій в слайдері

$(".small_first_circle").click(function(){

  $('.first_photo_big').css("display", "block");
  $('.wrapper_hide').css("display", "block");
  $('body').css("overflow", "hidden");
});

$(".close").click(function(){
  $('.first_photo_big').css("display", "none");
  $('.wrapper_hide').css("display", "none");
  $('body').css("overflow", "auto");

})


$(".small_second_circle").click(function(){

  $('.second_photo_big').css("display", "block");
  $('.wrapper_hide').css("display", "block");
  $('body').css("overflow", "hidden");
});

$(".close").click(function(){
  $('.second_photo_big').css("display", "none");
  $('.wrapper_hide').css("display", "none");
  $('body').css("overflow", "auto");

})


$(".small_seven_circle").click(function(){

  $('.third_photo_big').css("display", "block");
  $('.wrapper_hide').css("display", "block");
  $('body').css("overflow", "hidden");
});

$(".close").click(function(){
  $('.third_photo_big').css("display", "none");
  $('.wrapper_hide').css("display", "none");
  $('body').css("overflow", "auto");

})



/////блог фільтрування категорій

$.getJSON('/api/articles.json', function(json){
  var qwer = json.article[3];
  var q1 = qwer.category





  })

$('.category').click(function() {
  var $btn = $(this)
  var category = $btn.data('category')
  if(category === "all") {
    $('.card_place > div').fadeIn(450);
  } else {
    var $el = $('.' + category).fadeIn(450);

    $('#parent > div').not($el).hide();
  }
  $('.category').removeClass('active');
  $btn.addClass('active');
})



/
$(document).ready(function(){
  $('.root_h').hover(function(){
    $('.circle_h').css("background", "url(/uploads/about_school/main_image/2/PDAN2116.jpg)");
    $('.circle_h').css("background-size", "cover");
    $('.circle_h').css("transition", "1s");
  });
  $('.program_h').hover(function(){
    $('.circle_h').css("background", "url(/uploads/program/main_image/1/program.jpg)");
    $('.circle_h').css("transition", "1s");
  });
  $('.about_school_h').hover(function(){
    $('.circle_h').css("background", "url(/uploads/about_school/main_image/2/PDAN2116.jpg)");
    $('.circle_h').css("background-size", "cover");
    $('.circle_h').css("transition", "1s");
  });
  $('.rew').hover(function(){
    $('.circle_h').css("background", "url(/uploads/methodology/main_image/3/IMG_6292.jpg)");
    $('.circle_h').css("background-size", "cover");
    $('.circle_h').css("transition", "1s");
  });

})




$(document).ready(function(){
  $('.easyPaginateNav').css("width","0px")
})




//////  блог. dropdown категорій
$('.arrow').click(function(){
  $(this).toggleClass('active');
  $('.dropdown_m').css("display", "block");
  $('.arrow').css("display", "none");
  $('.arrow1').css("display", "block");
});

$('.arrow1').click(function(){
  $(this).toggleClass('active');
  $('.dropdown_m').css("display", "none");
  $('.arrow').css("display", "block");
  $('.arrow1').css("display", "none");
});





$('#select').on('click', function() {
  var getValue = $(this).text();
  // $('#select').text(getValue);
  $("#dropdown_1").css("display","block")

});

$('#a1').on('click', function() {
  var getValue = $(this).text();
  // var getValue = <%=params[:select]%>;
  $('#select').text(getValue);
});

$('#a2').on('click', function() {
  var getValue = $(this).text();
  $('#select').text(getValue);
});

$('#a3').on('click', function() {
  var getValue = $(this).text();
  $('#select').text(getValue);
});


$(document).ready(function() {
    $(".sand").click(function(){
        var count = [];
        $.each($("#select option:selected"), function(){
            count.push($(this).val());
        });

    });
});




$(document).ready(function() {

})

/////сховати відео в статті
// var iframe = document.getElementsByClassName("iframe_video");
// var src = iframe.src;
// if(!src) {
//   //hide the div
//   console.log(src)
//   // iframe[0].style.display = "none";
// }