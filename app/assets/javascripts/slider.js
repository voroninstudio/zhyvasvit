


$('.slider_car').slick({
    // mobileFirst: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: false, 
    autoplaySpeed: 6000,   
    dots: false,   
    infinite: true,
    cssEase: 'linear',
    draggable: true,
    prevArrow: $('.arrow_2'),
    nextArrow: $('.arrow_1'),
    responsive: [
    
    {
      breakpoint: 480,
      mobileFirst: true,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,

      }
    },
    {
      breakpoint: 1024,
      mobileFirst: true,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        mobileFirst: true
      }
    }
    
    ]
    
});


$('.slider_kinezo').slick({
    // mobileFirst: true,
    slidesToShow: 4,
    slidesToScroll: 1,    
    dots: false,
    infinite: true,
    cssEase: 'linear',
    draggable: true,
    arrows: true,
    prevArrow: $('.prev'),
    nextArrow: $('.next'),
    responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        mobileFirst: true
      }
    },
    {
      breakpoint: 480,
      mobileFirst: true,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,

      }
    }
    
    ]
    
});
$('.feedback_slider').slick({
    // mobileFirst: true,
    slidesToShow: 3,
    slidesToScroll: 1,    
    dots: false,
    infinite: true,
    cssEase: 'linear',
    draggable: true,
    prevArrow: $('.pp2'),
    nextArrow: $('.nn2'),
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        mobileFirst: true
      }
    },
    {
      breakpoint: 480,
      mobileFirst: true,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,

      }
    }
    
    ]
    
});

$('.slider').slick({
    // mobileFirst: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true, 
    autoplaySpeed: 1000,   
    dots: false,   
    infinite: true,
    cssEase: 'linear',
    draggable: true,
    prevArrow: $('.pp2'),
    nextArrow: $('.nn2'),
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        mobileFirst: true
      }
    },
    {
      breakpoint: 480,
      mobileFirst: true,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,

      }
    }
    
    ]
    
});



$('.slider_info').slick({
    // mobileFirst: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false, 
    autoplaySpeed: 0,   

    dots: false,   
    infinite: false,
    cssEase: 'linear',
    draggable: true,
    prevArrow: $('.prev_arr'),
    nextArrow: $('.next_arr'),
    responsive: [
    {
      breakpoint: 1366,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        mobileFirst: true
      }
    },
     {
      breakpoint: 768,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        mobileFirst: true
      }
    },
    
    ]
    
});