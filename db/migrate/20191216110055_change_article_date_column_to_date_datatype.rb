class ChangeArticleDateColumnToDateDatatype < ActiveRecord::Migration
  def change
    change_column :articles, :date, :date
  end
end
