class AddThirdDescriptionToDietologies < ActiveRecord::Migration
  def change
  	add_column :dietologies, :third_description, :text
  end
end
