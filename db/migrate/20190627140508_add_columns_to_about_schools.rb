class AddColumnsToAboutSchools < ActiveRecord::Migration
  def change
    add_column :about_schools, :new_way_description, :text
    add_column :about_schools, :base_title, :text
  end
end
