class CreateKlimatoterapy < ActiveRecord::Migration
  def change
    create_table :klimatoterapies do |t|
    	t.string :main_image
    	t.string :main_describe
    	t.string :main_title
    	t.string :first_title
    	t.text   :first_description
    	t.text   :second_description
    	t.text   :third_description
    	t.text   :fourth_description
    end
  end
end
 