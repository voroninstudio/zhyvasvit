class AddSelectToConsultation < ActiveRecord::Migration
  def change
    add_column :consultations, :select, :string
  end
end
