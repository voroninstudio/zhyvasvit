class CreateMainPage < ActiveRecord::Migration
  def change
    create_table :main_pages do |t|
    	t.string :main_title
    	t.string :main_title_list
    	t.text   :first_line_list
    	t.text   :second_line_list
    	t.text   :third_line_list
    	t.string :first_image
    	t.string :second_image
    	t.string :about_author_title
    	t.string :authors_name
    	t.text   :authors_describe
    	t.string :why_we_sick_title
    	t.text   :why_we_sick_describe
    	t.string :new_way_title
    	t.text   :new_way_describe
    	t.text   :video_title
    	t.string :ways_title
    	t.string :ways_describe
    	t.string :info_title
    	t.text   :info_describe
    	t.string :info_image
    	t.string :practise_image
    	t.string :practise_title
    	t.text   :practise_describe
    	t.string :help_title
    	t.text   :help_desciribe
    	t.string :help_image
    	t.string :feeling_title
    	t.string :headach_title
    	t.string :insomina_title
    	t.string :irritable_title
    	t.string :pain_title
    	t.string :pressure_title
    	t.string :fatigue_title
    	t.string :memory_title
    	t.string :depresion_title
    	t.string :weight_title
    	t.string :chronic_title
    	t.text   :chronic_describe
    	t.string :how_to_work_title
    	t.string :konsultation_title
    	t.text   :konsultation_describe
    	t.string :detoks_title
    	t.text   :detoks_describe
    	t.string :koretion_title
    	t.text   :koretion_describe
    	t.string :restore_title
    	t.text   :restore_describe
    	t.string :helping_title
    	t.text   :helping_describe
    	t.string :konsultation_title
    	t.text   :konsultation_describe
    	t.string :feedback_title
    end
  end
end
