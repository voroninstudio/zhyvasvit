class AddDetailsToProgram < ActiveRecord::Migration
  def change
    add_column :programs, :stage_1_image, :string
    add_column :programs, :stage_2_image, :string
    add_column :programs, :stage_3_image, :string
    add_column :programs, :stage_4_image, :string
  end
end
 