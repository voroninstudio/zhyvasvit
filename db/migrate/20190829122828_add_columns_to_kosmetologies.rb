class AddColumnsToKosmetologies < ActiveRecord::Migration
  def change
    add_column :kosmetologies, :third_number, :string
    add_column :kosmetologies, :third_title, :string
    add_column :kosmetologies, :third_describe, :text
    add_column :kosmetologies, :third_image, :string
  end
end
