class CreateBlog < ActiveRecord::Migration
  def change
    create_table :blogs do |t|
    	t.string :main_image
    	t.string :main_describe
    	t.string :main_title
    	t.string :first_title
    	t.text   :first_describe
    	t.string :first_category
    	t.string :second_category
    	t.string :third_category
    	t.string :fourth_category
    end
  end
end
