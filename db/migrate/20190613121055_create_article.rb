class CreateArticle < ActiveRecord::Migration
  def change
    create_table :articles do |t|
    	t.string :image    	
    	t.string :title
    	t.text   :describe
    	t.string :date
    end
  end
end
