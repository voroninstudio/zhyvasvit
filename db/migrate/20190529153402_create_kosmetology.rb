class CreateKosmetology < ActiveRecord::Migration
  def change
    create_table :kosmetologies do |t|
    	t.string :main_image
    	t.string :main_describe
    	t.string :main_title
    	t.string :first_title
    	t.text   :first_description
    	t.text   :second_description
    	t.text   :third_description
    	t.string :first_number
    	t.string :fiziology_title
    	t.text   :fiziology_describe
    	t.string :innovation_title
    	t.text   :innovation_first_describe
    	t.text   :innovation_second_describe
    	t.string :fiziology_image
    	t.string :rekonstruktive_image
    	t.string :second_number
    	t.string :rekonstruktive_title
    	t.text   :rekonstruktive_describe
    	t.string :botolino_title
    	t.text   :botolino_describe
    	t.string :gioluronov_acid_title
    	t.text   :gioluronov_acid_describe
    end
  end
end
 