class AddKlimateToMethodologies < ActiveRecord::Migration
  def change
    add_column :methodologies, :klimatology_title, :string
    add_column :methodologies, :klimatology_description, :text
  end
end
