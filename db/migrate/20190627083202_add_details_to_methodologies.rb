class AddDetailsToMethodologies < ActiveRecord::Migration
  def change
    add_column :methodologies, :kosmetology_title, :string
    add_column :methodologies, :kosmetology_description, :text
  end
end
