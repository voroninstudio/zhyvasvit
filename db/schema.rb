# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20191216120339) do

  create_table "about_schools", force: :cascade do |t|
    t.string "main_image"
    t.string "main_describe"
    t.string "main_title"
    t.string "first_title"
    t.text   "first_description"
    t.string "first_image"
    t.string "second_title"
    t.text   "second_description"
    t.string "third_title"
    t.text   "third_description"
    t.string "fourth_title"
    t.text   "fourth_description"
    t.text   "title_principes"
    t.text   "first_line_principes"
    t.text   "second_line_principes"
    t.text   "third_line_principes"
    t.text   "fourth_line_principes"
    t.text   "fifth_line_principes"
    t.string "second_image"
    t.text   "title_school"
    t.string "information"
    t.string "information_title"
    t.text   "information_description"
    t.string "practice"
    t.string "practice_title"
    t.text   "practice_description"
    t.string "vacation"
    t.string "vacation_title"
    t.text   "vacation_description"
    t.string "shypailo_name"
    t.string "shypailo_describe"
    t.text   "shypailo_first_title"
    t.text   "shypailo_first_describe"
    t.string "shypailo_image"
    t.text   "shypailo_way"
    t.string "help_title"
    t.text   "help_describe"
    t.string "step_one"
    t.string "step_one_title"
    t.text   "step_one_describe"
    t.string "step_two"
    t.string "step_two_title"
    t.text   "step_two_describe"
    t.text   "main_methodology"
    t.string "dietoterapy_title"
    t.text   "dietoterapy_describe"
    t.string "kinezoterapy_title"
    t.text   "kinezoterapy_describe"
    t.string "manual_title"
    t.text   "manual_describe"
    t.string "psyhoterapy_title"
    t.text   "psyhoterapy_describe"
    t.text   "new_way_description"
    t.text   "base_title"
  end

  create_table "articles", force: :cascade do |t|
    t.string   "image"
    t.string   "title"
    t.text     "describe"
    t.date     "date"
    t.string   "category"
    t.string   "video"
    t.text     "second_describe"
    t.text     "third_describe"
    t.string   "url_fragment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "short_description"
  end

  create_table "blogs", force: :cascade do |t|
    t.string "main_image"
    t.string "main_describe"
    t.string "main_title"
    t.string "first_title"
    t.text   "first_describe"
    t.string "first_category"
    t.string "second_category"
    t.string "third_category"
    t.string "fourth_category"
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",               null: false
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.string   "data_fingerprint"
    t.string   "type",              limit: 30
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "ckeditor_assets", ["type"], name: "index_ckeditor_assets_on_type"

  create_table "consultations", force: :cascade do |t|
    t.string "klients_name"
    t.string "phone_number"
    t.string "mail"
    t.string "select"
  end

  create_table "dietologies", force: :cascade do |t|
    t.string "main_image"
    t.string "main_describe"
    t.string "main_title"
    t.string "first_title"
    t.text   "first_description"
    t.text   "second_description"
    t.string "first_title_list"
    t.text   "first_line_description_first"
    t.text   "second_line_description_first"
    t.text   "third_line_description_first"
    t.text   "fourth_line_description_first"
    t.text   "fifth_line_description_first"
    t.text   "six_line_description_first"
    t.string "second_title_list"
    t.text   "first_line_description_second"
    t.text   "second_line_description_second"
    t.text   "third_line_description_second"
    t.text   "fourth_line_description_second"
    t.text   "bottom_title"
    t.text   "third_description"
  end

  create_table "form_configs", force: :cascade do |t|
    t.string   "type"
    t.text     "email_receivers"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "kinezoterapies", force: :cascade do |t|
    t.string "main_image"
    t.string "main_describe"
    t.string "main_title"
    t.string "first_title"
    t.text   "first_description"
    t.text   "title_technicks"
    t.text   "first_technick_title"
    t.text   "first_technick_describe"
    t.text   "first_list_title"
    t.text   "first_line_first"
    t.text   "first_line_second"
    t.text   "first_line_third"
    t.text   "first_describe_end"
    t.string "first_image"
    t.string "second_image"
    t.string "second_title"
    t.text   "second_description"
    t.string "third_image"
    t.string "third_title"
    t.text   "third_description"
  end

  create_table "klimatoterapies", force: :cascade do |t|
    t.string "main_image"
    t.string "main_describe"
    t.string "main_title"
    t.string "first_title"
    t.text   "first_description"
    t.text   "second_description"
    t.text   "third_description"
    t.text   "fourth_description"
  end

  create_table "kosmetologies", force: :cascade do |t|
    t.string "main_image"
    t.string "main_describe"
    t.string "main_title"
    t.string "first_title"
    t.text   "first_description"
    t.text   "second_description"
    t.text   "third_description"
    t.string "first_number"
    t.string "fiziology_title"
    t.text   "fiziology_describe"
    t.string "innovation_title"
    t.text   "innovation_first_describe"
    t.text   "innovation_second_describe"
    t.string "fiziology_image"
    t.string "rekonstruktive_image"
    t.string "second_number"
    t.string "rekonstruktive_title"
    t.text   "rekonstruktive_describe"
    t.string "botolino_title"
    t.text   "botolino_describe"
    t.string "gioluronov_acid_title"
    t.text   "gioluronov_acid_describe"
    t.string "third_number"
    t.string "third_title"
    t.text   "third_describe"
    t.string "third_image"
  end

  create_table "main_pages", force: :cascade do |t|
    t.string "main_title"
    t.string "main_title_list"
    t.text   "first_line_list"
    t.text   "second_line_list"
    t.text   "third_line_list"
    t.string "first_image"
    t.string "second_image"
    t.string "about_author_title"
    t.string "authors_name"
    t.text   "authors_describe"
    t.string "why_we_sick_title"
    t.text   "why_we_sick_describe"
    t.string "new_way_title"
    t.text   "new_way_describe"
    t.text   "video_title"
    t.string "ways_title"
    t.string "ways_describe"
    t.string "info_title"
    t.text   "info_describe"
    t.string "info_image"
    t.string "practise_image"
    t.string "practise_title"
    t.text   "practise_describe"
    t.string "help_title"
    t.text   "help_desciribe"
    t.string "help_image"
    t.string "feeling_title"
    t.string "headach_title"
    t.string "insomina_title"
    t.string "irritable_title"
    t.string "pain_title"
    t.string "pressure_title"
    t.string "fatigue_title"
    t.string "memory_title"
    t.string "depresion_title"
    t.string "weight_title"
    t.string "chronic_title"
    t.text   "chronic_describe"
    t.string "how_to_work_title"
    t.string "konsultation_title"
    t.text   "konsultation_describe"
    t.string "detoks_title"
    t.text   "detoks_describe"
    t.string "koretion_title"
    t.text   "koretion_describe"
    t.string "restore_title"
    t.text   "restore_describe"
    t.string "helping_title"
    t.text   "helping_describe"
    t.string "feedback_title"
    t.string "video"
  end

  create_table "manuals", force: :cascade do |t|
    t.string "main_image"
    t.string "main_describe"
    t.string "main_title"
    t.string "first_title"
    t.text   "first_description"
    t.text   "title_technicks"
    t.text   "first_title_technicks"
    t.text   "first_describe_technicks"
    t.text   "french_technick_title"
    t.text   "french_technick_first_line"
    t.text   "french_technick_second_line"
    t.text   "french_technick_third_line"
    t.string "first_image"
    t.string "second_image"
    t.string "second_title"
    t.text   "second_description"
    t.string "third_image"
    t.string "third_title"
    t.text   "third_description"
  end

  create_table "methodologies", force: :cascade do |t|
    t.string "main_image"
    t.string "main_describe"
    t.string "main_title"
    t.string "first_title"
    t.text   "first_description"
    t.text   "second_list_description"
    t.string "task_title"
    t.text   "task_description"
    t.string "first_image"
    t.text   "second_title"
    t.string "dieto_title"
    t.text   "dieto_description"
    t.string "kinezo_title"
    t.text   "kinezo_description"
    t.string "manual_title"
    t.text   "manual_description"
    t.string "psyho_title"
    t.text   "psyho_description"
    t.string "kosmetology_title"
    t.text   "kosmetology_description"
    t.string "klimatology_title"
    t.text   "klimatology_description"
  end

  create_table "page_translations", force: :cascade do |t|
    t.integer  "page_id",    null: false
    t.string   "locale",     null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "name"
    t.text     "content"
    t.string   "url"
    t.string   "h1_text"
  end

  create_table "pages", force: :cascade do |t|
    t.string   "type"
    t.integer  "sorting_position"
    t.string   "name"
    t.text     "content"
    t.string   "url"
    t.string   "h1_text"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "programs", force: :cascade do |t|
    t.string "main_image"
    t.string "main_describe"
    t.string "main_title"
    t.string "first_title"
    t.string "first_image"
    t.text   "first_title_description"
    t.text   "first_description"
    t.string "second_title"
    t.text   "second_description"
    t.string "first_title_list"
    t.text   "first_line_list"
    t.text   "second_line_list"
    t.text   "third_line_list"
    t.text   "fourth_line_list"
    t.text   "fifth_line_list"
    t.text   "six_line_list"
    t.text   "seventh_line_list"
    t.text   "eight_line_list"
    t.text   "nine_line_list"
    t.text   "ten_line_list"
    t.text   "eleven_line_list"
    t.text   "twelve_line_list"
    t.text   "third_title_right"
    t.text   "button_describe"
    t.string "about_program_title"
    t.text   "a_p_description"
    t.string "a_p_first_title"
    t.text   "a_p_first_descrpiton"
    t.string "a_p_second_title"
    t.text   "a_p_second_descrpiton"
    t.string "a_p_third_title"
    t.text   "a_p_third_descrpiton"
    t.string "a_p_image"
    t.string "a_p_fourth_title"
    t.string "step_one"
    t.string "step_one_title"
    t.text   "step_one_describe"
    t.string "step_two"
    t.string "step_two_title"
    t.text   "step_two_describe"
    t.string "stages_title"
    t.text   "stages_describe"
    t.string "stage_1"
    t.string "stage_1_title"
    t.text   "stage_1_first_line"
    t.text   "stage_1_second_line"
    t.text   "stage_1_third_line"
    t.string "stage_2"
    t.string "stage_2_title"
    t.text   "stage_2_describe"
    t.string "stage_3"
    t.string "stage_3_title"
    t.text   "stage_3_first_line"
    t.text   "stage_3_second_line"
    t.text   "stage_3_third_line"
    t.text   "stage_3_describe"
    t.string "stage_4"
    t.string "stage_4_title"
    t.text   "stage_4_describe"
    t.string "stage_1_image"
    t.string "stage_2_image"
    t.string "stage_3_image"
    t.string "stage_4_image"
  end

  create_table "psychoterapies", force: :cascade do |t|
    t.string "main_image"
    t.string "main_describe"
    t.string "main_title"
    t.string "first_title"
    t.text   "first_description"
    t.string "list_title"
    t.text   "first_list_description"
    t.text   "second_list_description"
    t.text   "third_list_description"
    t.text   "fourth_list_description"
  end

  create_table "seo_tag_translations", force: :cascade do |t|
    t.integer  "seo_tag_id",  null: false
    t.string   "locale",      null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "title"
    t.text     "keywords"
    t.text     "description"
  end

  create_table "seo_tags", force: :cascade do |t|
    t.string  "page_type"
    t.integer "page_id"
    t.string  "title"
    t.text    "keywords"
    t.text    "description"
  end

  create_table "sitemap_elements", force: :cascade do |t|
    t.string   "page_type"
    t.integer  "page_id"
    t.boolean  "display_on_sitemap"
    t.string   "changefreq"
    t.float    "priority"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "subscribers", force: :cascade do |t|
    t.string "email"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
