Rails.application.routes.draw do
  get 'robots', format: 'txt', as: :robots_txt, to: 'robots#robots_txt'
  mount Cms::Engine => '/', as: 'cms'
  mount Ckeditor::Engine => '/ckeditor'

  devise_for :users, module: 'users', path: 'users', path_names: {
    sign_in: 'login',
    sign_out: 'logout',
  }

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  controller :pages do
    root action: :index
    get "about-school",	 action: :about_school, as: :about_school
    get "program", 			 action: :program, as: :program
    get "methodology", 	 action: :methodology, as: :methodology
    get "contacts",			 action: :contacts, as: :contacts
    get "dietology",     action: :dietology, as: :dietology
    get "kinezoterapy",  action: :kinezoterapy, as: :kinezoterapy
    get "manual",        action: :manual, as: :manual
    get "psychoterapy",  action: :psychoterapy, as: :psychoterapy
    get "blog", 				 action: :blog, as: :blog
    get "blog/:id",      action: :article, as: :article
    get "klimatoterapy", action: :klimatoterapy, as: :klimatoterapy
    get "kosmetology",   action: :kosmetology, as: :kosmetology
    post "consultation", action: :consultation, as: :consultation
  end

  resources :subscribers, only: [:create]

  namespace :api do
    resources :articles, only: [:index]
  end

  match '*url', to: 'application#render_not_found', via: :all
end






